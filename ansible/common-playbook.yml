---

    - include_vars: variables.yml
    
    - name: Set Hostname
      raw: hostnamectl set-hostname "{{ hostname }}"

    - name: Register with RedHat and auto-subscribe to available content.
      redhat_subscription:
        state: present
        username: "{{ redhatusername }}"
        password: "{{ redhatpassword }}"
        auto_attach: true
      
#Below was used when the above command would hang and never return I kept this here in case that happens again 
      #    - name: Register VM with "{{ redhatusername }}" RedHat and auto-subscribe to available content.
      #      raw: sudo subscription-manager register --username "{{ redhatusername }}" --password "{{ redhatpassword }}" --auto-attach

    - name: Ensure dependencies for cri-o are installed
      yum:
              name:
                      - conmon
                      - container-selinux
                      - containernetworking-plugins
                      - runc
                      - skopeo-containers
                      - socat
                      - git  
              state: present
              update_cache: true

    - name: Install CRI-O from rpm on localhost
      yum:
              name:
                      - /vagrant/cri-o-1.18.1-1.1.el8.x86_64.rpm
              state: present
              update_cache: true

    - name: Set up CRI-O config file
      shell:
        cmd: |
          cat << EOF > /etc/crio/crio.conf
          [crio]
          storage_driver = "overlay2"
          [crio.api]
          listen = "/var/run/crio/crio.sock"
          stream_address = ""
          stream_port = "10010"
          stream_enable_tls = false
          stream_tls_cert = ""
          stream_tls_key = ""
          stream_tls_ca = ""
          file_locking = false
          [crio.runtime]
          runtime = "/usr/bin/runc"
          runtime_untrusted_workload = ""
          default_workload_trust = "trusted"
          no_pivot = false
          conmon = "/usr/bin/conmon"
          conmon_env = [
              "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
          ]
          selinux = true
          seccomp_profile = "" 
          apparmor_profile = "crio-default"
          cgroup_manager = "cgroupfs"
          default_capabilities = [
              "CHOWN",
              "DAC_OVERRIDE",
              "FSETID",
              "FOWNER",
              "NET_RAW",
              "SETGID",
              "SETUID",
              "SETPCAP",
              "NET_BIND_SERVICE",
              "SYS_CHROOT",
              "KILL",
          ]
          hooks_dir_path = "/usr/share/containers/oci/hooks.d"
          default_mounts = [
              "/usr/share/rhel/secrets:/run/secrets",
          ]
          pids_limit = 1024
          log_size_max = -1
          read_only = false
          log_level = "error"
          uid_mappings = ""
          gid_mappings = ""
          [crio.image]
          default_transport = "docker://"
          pause_image = "k8s.gcr.io/pause-amd64:3.1"
          pause_command = "/pause"
          signature_policy = ""
          image_volumes = "mkdir"
          registries = [
            "docker.io",
            "registry.hub.docker.com",
            "k8s.gcr.io",
          ]
          [crio.network]
          network_dir = "/etc/cni/net.d/"
          plugin_dir = "/opt/cni/bin"
          EOF

      args:
        executable: /bin/bash
    
    - name: Start Crio Now
      raw: systemctl enable --now crio

    - name: Turn off the Firewall
      systemd:
         name: firewalld
         state: stopped
         enabled: false
    
    - name: Remove swapfile from /etc/fstab
      mount:
              name: "{{ item }}"
              fstype: swap
              state: absent
      with_items:
       - swap
       - none

    - name: Disable swap
      command: swapoff -a
      when: ansible_swaptotal_mb >0

    - name: Put SELinux in permissive mode, logging actions that would be blocked.
      selinux:
        policy: targeted
        state: permissive

    - name: Add Kubernets Repo
      yum_repository:
        name: Kubernetes
        description: Kubernetes YUM repo
        file: kubernetes.repo
        baseurl: https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
        gpgcheck: yes
        enabled: yes
        gpgkey: 
         - https://packages.cloud.google.com/yum/doc/yum-key.gpg 
         - https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
    
    - name: Install Kubernetes packages
      yum:
        name:
          - kubelet
          - kubeadm
          - kubectl
        state: present
    
    - name: Start kubelet
      systemd:
         name: kubelet
         daemon_reload: yes
         enabled: true
    
    - name: Add overlay Files system
      modprobe:
         name: overlay
         state: present
    
    - name: Add br_netfilter
      modprobe:
         name: br_netfilter
         state: present

    - name: Set up kubernetes IP tables in int conf file
      shell:
        cmd: |
          cat > /etc/sysctl.d/99-kubernetes-cri.conf <<EOF
          net.bridge.bridge-nf-call-iptables  = 1
          net.ipv4.ip_forward                 = 1
          net.bridge.bridge-nf-call-ip6tables = 1
          EOF
      args:
        executable: /bin/bash
     
    - sysctl:
        name: net.ipv4.ip_forward
        value: '1'
        sysctl_set: yes
        state: present
        reload: yes      
